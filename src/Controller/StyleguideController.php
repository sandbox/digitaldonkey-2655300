<?php
/**
 * @file
 * Contains \Drupal\kss_styleguide\Controller\StyleguideController.
 */

namespace Drupal\kss_styleguide\Controller;
use Drupal\Core\Controller\ControllerBase;

/**
 * StyleguideController.
 */
class StyleguideController extends ControllerBase {

  private $requestStack = FALSE;

  /**
   * Function getRequest().
   */
  protected function getRequest() {
    if (!$this->requestStack) {
      $this->requestStack = \Drupal::service('request_stack');
    }
    return $this->requestStack->getCurrentRequest();
  }

  /**
   * Generates an example page.
   */
  public function index() {

    $kss_html = $this->getContent($this->styleguidepath() . '/index.html');

    return array(
      '#type' => 'inline_template',
      '#template' => $kss_html . $this->createdComment(),
      '#cache' => array(
        'max-age' => 0,
      ),
    );
  }


  /**
   * Generates an example page.
   */
  public function subindex() {

    $sub_path = $this->getRequest()->attributes->get('node');

    $kss_html = $this->getContent($this->styleguidepath() . '/' . $sub_path . '.html');

    return array(
      '#type' => 'inline_template',
      '#template' => $kss_html . $this->createdComment(),
      '#cache' => array(
        'max-age' => 0,
      ),
    );
  }


  /**
   * Rewrite urls.
   *
   * @param string $html
   *   File Contents.
   *
   * @return string
   *   File content striped our between header and footer Tags.
   */
  private function rewriteUrls($html) {

    // TODO: THIS WILL TARGET ALL LINKS, NOT ONLY KSS NAV ELEMENTS.

    $html = str_replace('href="./"', 'href="/styleguide"', $html);
    $html = preg_replace('/href="(.*).html(.*)"/im', 'href="/styleguide/' . '${1}${2}' . '"', $html);
    return $html;
  }


  /**
   * Remove header and footer of KSS file.
   *
   * @param string $source
   *   Filename of KSS node HTML file.
   *
   * @return string
   *   File content striped our between header and footer Tags.
   */
  private function getContent($source) {
    $html = file_get_contents($source);
    $conf = \Drupal::config('kss_styleguide.settings');

    // Remove Header and Footer.
    $start = strpos($html, $conf->get('split_start')) + strlen($conf->get('split_start'));
    $end = strpos($html, $conf->get('split_end'));
    $html = substr($html, $start, $end - $start);

    // Rewrite the urls.
    return $this->rewriteUrls($html);
  }


  /**
   * Get styleguide path.
   *
   * Removes trailing and ending slashes if required.
   */
  protected function styleguidepath() {
    $path = \Drupal::config('kss_styleguide.settings')->get('path');

    if (substr($path, 0, 1) === "/") {
      $path = substr($path, 1);
    }

    if (substr($path, -1) === "/") {
      $path = substr($path, 0, -1);
    }

    return DRUPAL_ROOT . '/' . $path;
  }


  /**
   * Get created_comment.
   *
   * Removes trailing and ending slashes if required.
   */
  protected function createdComment() {
    return '<!-- Styleguide rendered at ' . date('Y-m-d h:i:s') . ' -->';
  }

}
