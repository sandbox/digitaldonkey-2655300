# KSS Styleguide

The kss_styleguide module is a wrapper to render a KSS-node styleguide within drupal header and footer.
 
See https://github.com/kss-node/kss-node

### Access

Kss styleguide will be rendered the fixed url /styleguide for everyone with "access content" permission. You may change this in kss_styleguide.routing.yml.


### Settings
 
The KSS HTML output is just split apart. Make sure you KSS-Template containing the following HTML Comments

 * split_start: &lt;!-- BODY START --&gt;
 * split_end: &lt;!-- BODY END --&gt;

The styleguide source must be a kss rendered template residing in "themes/git_repos/donkeymedia_theme/styleguide".

You may adapt these values in kss_styleguide.settings.yml BEFORE enabling the module. 

### Note

Please note that there are also references to kss css and javascript files in the template html--kss_styleguide.html.twig
